# sunona

Color Pallete
![Palette](https://user-images.githubusercontent.com/74754285/232721664-8bd775fb-0839-4f56-ab5f-d54d367ad4b8.png)
### CSS HEX
-indochine: #cb7b05  
-martini: #b6a7a9  
-irish-coffee: #523221  
-fuzzy-wuzzy brown: #c25c63  
-mischka: #ebe8ec  
-sandal: #b2806d  
-nevada: #62707b  
-mischka: #d1d4dc  
-geyser: #d4dce2  


### SCSS RGB
-indochine: rgba(203,123,5,1)  
-martini: rgba(182,167,169,1)  
-irish-coffee: rgba(82,50,33,1)  
-fuzzy-wuzzy brown: rgba(194,92,99,1)  
-mischka: rgba(235,232,236,1)  
-sandal: rgba(178,128,109,1)  
-nevada: rgba(98,112,123,1)  
-mischka: rgba(209,212,220,1)  
-geyser: rgba(212,220,226,1)  
