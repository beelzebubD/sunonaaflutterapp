import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();

}

class _MainScreenState extends State<MainScreen> {

  String mode = 'User';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(

          children: [
            Container(
              margin: EdgeInsets.only(right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Switch(value: false, onChanged: (bool value){
                    mode = value?'Volunteer':'User';
                  }),
                  Text(mode,
                  ),
                ],
              ),
            ),
            Container(
                margin: const EdgeInsets.all(30),
                child: const Image(
                    image: AssetImage('assets/images/abstract_image.jpg'))),
            Container(
              margin: const EdgeInsets.fromLTRB(30, 10, 30, 0),
              child: const Text(
                'What\'s your struggle?',
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Colors.redAccent,
                ),
              ),
            ),
            Container(
                padding:
                const EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 20),
                child: Container(
                    padding: const EdgeInsets.all(10),
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Colors.grey,
                            borderRadius:
                            BorderRadius.all(Radius.circular(12))),
                        padding: const EdgeInsets.all(10),
                        child: Container(
                          padding: EdgeInsets.zero,
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                              BorderRadius.all(Radius.circular(8)),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.white,
                                    blurRadius: 10,
                                    spreadRadius: 10)
                              ]),
                          width: double.infinity,
                          child: Container(
                              margin: const EdgeInsets.all(0),
                              child: Column(
                                children: [
                                  Container(
                                    margin: const EdgeInsets.all(20),
                                    child: TextFormField(
                                      maxLines: null,
                                      decoration: const InputDecoration(
                                        hintText:
                                        'What you type helps us match you with the right people.\n\n',
                                        hintMaxLines: 10,
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.fromLTRB(
                                        20, 10, 20, 20),
                                    child: InkWell(
                                      onTap: () {
                                        print('Tapped');
                                      },
                                      child: const Text(
                                        'Not sure what to say? Get help here >>',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )),
                        )
                    )
                )
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(30, 10, 30, 0),
              child: SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(vertical: 15),
                  ),
                  onPressed: () {
                    print('Tapped');
                  },
                  child: const Text('Get Matched',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
