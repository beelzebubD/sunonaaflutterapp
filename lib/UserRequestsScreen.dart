import 'package:flutter/material.dart';

class UserRequestsScreen extends StatefulWidget {
  const UserRequestsScreen({Key? key}) : super(key: key);

  @override
  State<UserRequestsScreen> createState() => _UserRequestsScreenState();
}

class _UserRequestsScreenState extends State<UserRequestsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('User Requests'),
      ),
      body: ListView.builder(itemBuilder: (context, index){
        return RequestItem('Name');
      }, itemCount: 40,),
    );
  }
}

Widget RequestItem(String name) {
  return Padding(
    padding: const EdgeInsets.only(bottom: 10),
    child: Container(
      margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 5,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Padding(
        padding: EdgeInsets.fromLTRB(15, 10, 10, 10),
        child: Row(
          children: [
            Text('Name',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),),
            SizedBox(width: 10,),
            Spacer(),
            GestureDetector(
              onTap:(){

              },
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.greenAccent.shade100,
                  ),
                  padding: EdgeInsets.all(12),
                  child: Icon(Icons.check, size: 30, color: Colors.green,),
                )
              ),
            ),
            SizedBox(width: 10,),
            GestureDetector(
              onTap:(){

              },
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.redAccent.shade100,
                    ),
                    padding: EdgeInsets.all(12),
                    child: Icon(Icons.close, size: 30, color: Colors.red,),
                  )
              ),
            )
          ],
        ),
      ),
    ),
  );
}
